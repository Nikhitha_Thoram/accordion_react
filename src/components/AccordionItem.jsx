import React from "react";

function AccordionItem({ title, description, onClick, isOpen}) {
  return (
    <div className="accordion_item">
      <div className="accordion_header" onClick={onClick}>
        <h3>{title}</h3>
        <span className="icon">{isOpen ? "-" : "+"}</span>
      </div>
     <div className={`accordion_content ${isOpen ? "open" : "closed"}`}>
        {description}</div>
    </div>
  );
}

export default AccordionItem;
