import React from 'react';

function Accordion({children}) {
  return(
    <div className='accordion_container'>
      {children}
    </div>
  )

}

export default Accordion;
