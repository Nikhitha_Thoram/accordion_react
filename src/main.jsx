import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'


const initialOpenIndices =[1,2];
const isMultiple=false;

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App initialOpenIndices={initialOpenIndices} isMultiple={isMultiple}/>
  </React.StrictMode>,
)
