import { useState } from "react";
import "./App.css";
import Accordion from "./components/Accordion";
import AccordionItem from "./components/AccordionItem";
import { Items } from "./components/Items";

function App({ initialOpenIndices, isMultiple }) {
  const [activeAccordion, setActiveAccordion] = useState(
    Array.isArray(initialOpenIndices) && initialOpenIndices
      ? isMultiple
        ? initialOpenIndices
        : initialOpenIndices[0]
      : -1
  );
  
  const handleAccordionClick = (index) => {
    if (isMultiple) {
      if (Array.isArray(activeAccordion)) {
        const newActiveAccordion = [...activeAccordion];
        const clickedIndex = newActiveAccordion.indexOf(index);
        if (clickedIndex > -1) {
          newActiveAccordion.splice(clickedIndex, 1);
        } else {
          newActiveAccordion.push(index);
        }
        setActiveAccordion(newActiveAccordion);
      } else {
        setActiveAccordion([index]);
      }
    } else {
      setActiveAccordion(activeAccordion === index ? null : index);
    }
  };

  return (
    <div className="main_container">
      <p className="heading">Getting Started</p>
      <Accordion>
        {Items.map((item, index) => (
          <AccordionItem
            title={item.title}
            description={item.description}
            key={index}
            isOpen={
              Array.isArray(activeAccordion)
                ? activeAccordion.includes(index)
                : activeAccordion === index
            }
            onClick={() => handleAccordionClick(index)}
          />
        ))}
      </Accordion>
    </div>
  );
}

export default App;
